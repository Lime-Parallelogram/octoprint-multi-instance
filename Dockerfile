FROM python
ENV PYTHONUNBUFFERED=1
ENV PYTHONIOENCODING=UTF-8

WORKDIR /usr/share/app

COPY requirements.txt /tmp/

COPY src .
RUN rm opmi_backend/.env
RUN mv opmi_backend/.env.prod opmi_backend/.env
COPY angular_build /var/www/angular_build

RUN pip install --no-cache-dir -r /tmp/requirements.txt

RUN rm -r /usr/share/app/opmi_backend/persistent
RUN mkdir /usr/share/app/opmi_backend/persistent
VOLUME /usr/share/app/opmi_backend/persistent

COPY gunicorn.conf.py .

RUN apt update && apt install -y supervisor nginx
COPY supervisord.conf /etc/supervisor/conf.d/supervisord.conf
COPY nginx.conf /etc/nginx/nginx.conf

EXPOSE 5000

COPY start.sh .
CMD ["sh", "./start.sh"]