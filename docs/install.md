# OctoPrint Multi-Instance : Installation
This guide assumes that you already have your kiosk system setup and working. You will need to have access to an administration account - with which you can make changes to the system. In addition to your kiosk UI, you will also need `docker` installed.

> 🛈 If you do not yet have a system set up, take a look at [docs/suggested_system.md](suggested_system.md) for a guide to setting up a kiosk machine with Rocky Linux.

---
## Docker Installation
The easiest way to run OctoPrint Multi-Instance is via Docker. 

You should first create a volume to store any persistent data within OctoPrint Multi-Instance.
```bash
docker volume create opmi_coordinator_data
```

You can then spin up the docker container:
```bash
docker run -d \
--name opmi_coordinator \
--restart always \
--security-opt label=disable \
--label=com.centurylinklabs.watchtower.enable=true \
--label=uk.limeparallelogram.opmi.opmi_related=true \
-v /var/run/docker.sock:/var/run/docker.sock \
-v opmi_coordinator_data:/usr/share/app/opmi_backend/persistent \
-v /dev:/dev \
-p 5000:80 \
limeparallelogram/opmi:stable
```

The flag `--security-opt label=disable` performs the same function as `--privileged` and is necessary to allow manipulation of Docker via the socket. On non-SELinux enabled systems, you may need to use `--privileged` instead.

The custom label `uk.limeparallelogram.opmi.opmi_related=true` is set to ensure that any container management performed by OctoPrint Multi-Instance does not modify your unrelated, manually-created containers.

The label `com.centurylinklabs.watchtower.enable=true` is used optionally to enable automatic updates for this container. See installing details about installing `Watchtower` below.

Test that you can access the OctoPrint Multi-Instance interface at http://localhost:5000/.

> ⚠️ Note: The 'stable' channel will be the same as the 'beta' channel until the release of the first stable release.

---

## Optional: Watchtower
[Watchtower](https://containrrr.dev/watchtower/) is an additional docker container that you may want to run alongside OctoPrint Multi-Instance. This will enable automatic updates for specified Docker containers so you can be sure that you are always running the latest release. Start this using the following command:
```bash
docker run -d \
--name watchtower \
--restart always \
--security-opt label=disable \
--label=uk.limeparallelogram.opmi.opmi_related=true \
-e WATCHTOWER_LABEL_ENABLE=true \
-v /var/run/docker.sock:/var/run/docker.sock \
containrrr/watchtower
```
You may notice the `WATCHTOWER_LABEL_ENABLE` variable in the command above. This is important as is means watchtower will only monitor and update those containers that have been deliberately given the necessary label. We do not want the OctoPrint containers themselves to be automatically updated as this could interrupt printing.