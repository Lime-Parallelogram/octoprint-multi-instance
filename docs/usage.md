# Using OctoPrint Multi-Instance
Obviously we hope that OctoPrint Multi-Instance will be a self-explanatory, user-friendly system however the system is still in early stages of development so things may not be perfect yet. This guide should help you get started using the system.

## The Main Interface
![A screenshot showing the main interface](img/main_page_annotated.png)
**(1)** All of your configured printers are listed here with their configured colour and initial.  
**(2)** You can access the settings page by clicking the gear icon.

## Printer Configuration
![A screenshot of the printers configuration page](img/printer_settings_annotated.png)
**(1)** Currently configured printers will be listed with their name, address and port number.  
**(2)** Printers showing this symbol are managed by OPMI. See the 'Manage Mode' information below.  
**(3)** Edit: Make changes to an existing container. *Note: The container will be restarted and prints disrupted when edits are saved*.  
**(4)** Re-create: Manually force OPMI to recreate the container even without any changes being made.  
**(5)** Delete: Delete this printer. If this is a managed instance, any data not stored in 'reusable volumes' will be lost.  
**(6)** Add a new printer to the list.  

## A Printer's Settings
![A screenshot showing the edit page for printers](img/printer_edit_annotated.png)
**(1)** Cosmetic settings: These are purely used when displaying printers in the OctoPrint Multi-Instance interface.  
**(2)** Address and port: These may point to an existing OctoPrint server or left as `localhost` if planning to use the management features. If you are connecting to an external OctoPrint server, be sure to enable embedding in the settings for that server.  
**(7)** Save: Note, updating printers will update any currently active prints.
#### Management Mode
Managed mode is where the OctoPrint Multi-Instance system itself is able to run and manage instances of OctoPrint.  
**(3)** Enable managed mode.  
**(4)** Serial port: Select a serial port to associate with this printer. Pressing the refresh button from within the serial port selector interface will mark any newly detected printers with a (*).  
**(5)** Automatic configuration mode: If set, the system will automatically change some settings within the OctoPrint instance. If 'Essentials Only' is selected, only the 'enable embedding' and 'additional serial ports' settings is updated. If 'Essentials & Theme' is selected, name and colour is automatically updated also.  
**(6)** Additional Volumes: Select any 'Reusable Volumes' that should be available to this instance. You may not select multiple volumes that have the same container path. Volumes can be configured via the 'Reusable Volumes' tab.

# Reusable volumes
Volumes can be created, edited and deleted just like printers. They can currently be configured with the following properties:
![A screenshot showing the volume edit dialogue](img/volume_edit_annotated.png)
**(1)** The name of this volume. This is not used other than as an identifier.  
**(2)** The location that this directory should be mapped to within the OctoPrint container. The default value represents the OctoPrint uploads directory (where the GCODE files are listed from by default).  
**(3)** The path of the actual system where this should point to. If you have mounted a shared folder, you could set the host path to this location - otherwise you could choose a location within your home folder.  