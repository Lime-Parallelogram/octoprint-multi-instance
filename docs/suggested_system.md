# Suggested System Setup
This document will define a possible component stack that you may choose to use for your own setup. I have chosen modern linux technologies to try and make the system as future-proof as possible. Having used Linux for a number of years, I've experienced most of these things before however I know that the range of available projects and technologies is vast so if you have any suggestions about more suitable ways to do things then please let me know - I want to have confidence that I am suggesting the **best tools for the job**.

## Operating System
For the operating system, I have chosen **Rocky Linux**. I've chosen to use an rpm based distribution because they have the `gnome-kiosk-script` package available in the repos whereas I can't seem to find the equivalent in the Debian repos. Rocky offers a very long support lifecycle which will ensure long term security of your system without needed to worry about major version updates. In previous versions of this guide, I suggested **Fedora Silverblue**; this too is a good option which offers an immutable root filesystem and thus provides additional security. An important note is that while using Silverblue, I experienced permission issues trying to use a mounted SMB share from within the OctoPrint containers.

### Download
You can download and install **Rocky Linux** from [here](https://rockylinux.org/).

### Install
The installer will prompt you to select your target disk and localization options. You can choose to encrypt the disk at this stage however I would *not recommend* this as you will need to enter a password whenever your system reboots.

From the initial install screen, you should also create an admin user and password - be sure to check 'Make this user administrator'. You can then proceed and install the system.  
Once the base system is installed, log in with your new user and install a graphical environment using:
```bash
sudo dnf group install "Server with GUI"
sudo systemctl set-default graphical
```

## Dependencies
These are the packages that must be installed for OctoPrint Multi-Instance to work properly on your newly-installed system. The following installation commands are provided for Rocky Linux however the equivalent packages will be available on all RHEL-based Linux distributions (Docker is available for almost every system).

To install docker, run the following in terminal:
```
sudo yum install -y yum-utils
sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
sudo yum install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin

sudo systemctl start docker
```
> Note: Instructions are given as stated by [the official docker installation documentation](https://docs.docker.com/engine/install/centos/).

Additionally, you may want to add your admin user to the `docker` group so as to allow control of Docker without needing `sudo`. Run the command:
```
sudo usermod -a -G docker <username>
```

To install gnome-kiosk, run the following in terminal:
```bash
sudo dnf install gnome-kiosk gnome-kiosk-script-session
```
You have now installed Docker as well as GNOME Kiosk. GNOME Kiosk is preferable to, for example, installing a bare window manger like Openbox because it supports Wayland easily and because it supports having the full GNOME desktop environment alongside the kiosk mode. This makes for easy configuration and maintenance.

You should now reboot your system.

## OctoPrint Multi-Instance Installation
At this point, I would recommend following the instructions described in [docs/install.md](install.md). You can then return here and complete the remaining steps to finalize the kiosk setup.

## Additional User
You can now add a new user. This will be a non-privileged user that end users will interact with. Visit the GNOME Settings (accessed by clicking on the icons in the top right corner) and open the 'Users' page. Choose 'Add User' and enter the relevant information. Be sure to 'Set password now'.
![A screenshot showing the new-user creation page](img/new_user_create.png)
Once the user has been created, you should see a toggle to enable Automatic Login. While it may be tempting to enable automatic login, once this is enabled I cannot find a way to escape from the kiosk account and return to the admin login. You should thus **NOT ENABLE AUTOMATIC LOGIN UNLESS YOU ARE SURE YOU KNOW WHAT YOU ARE DOING**.

Before logging out, ensure that OctoPrint Multi-Instance is accessible as described in the installation instructions. 

You may now log out of your administration account.

Select the new user and click the cog in the bottom right corner.
![A screenshot of the GNOME login screen](img/login.png)
Choose 'Kiosk Script Session (Wayland Display Server)' and enter your password.

You should now be presented with a full-screen text editor. Delete everything and replace it with the following. Press Ctrl+S to save and then reboot your machine.

```sh
#!/bin/sh

if [ ! "$(pidof firefox)" ]
then
   firefox --kiosk http://localhost:5000/
fi

sleep 1.0
exec "$0" "$@"
```

If you ever need to modify this, you will need to do so using your administration login. You can find the file located in `/home/<username>/.local/bin/gnome-kiosk-script` where `<username>` is the name of your kiosk user. To open this file in a terminal-based editor you can enter the command
```bash
sudo nano /home/<username>/.local/bin/gnome-kiosk-script
```

## Exit Kiosk Mode
As far as I know, the only way to return to the admin account once the kiosk has been opened is to reboot the machine. If you accidentally enable automatic login to your kiosk account, you may have to boot a live system (such as the Ubuntu installer) from USB and then modify `/etc/gdm/custom.conf` to set `AutomaticLoginEnable` to `false`.