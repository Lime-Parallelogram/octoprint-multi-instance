#!/bin/bash

cd src-ui
ng build

rm -r ../angular_build
mv dist/opmi_ui ../angular_build

cd ..
docker build -t limeparallelogram/opmi:beta .