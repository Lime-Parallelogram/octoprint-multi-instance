from django.db import models

# Create your models here.
class Volume(models.Model):
    name = models.CharField(max_length=50)
    container_path = models.CharField(max_length=250)
    host_path = models.CharField(max_length=250)

    smb = models.BooleanField(default=False)
    smb_path = models.CharField(max_length=250, null=True, blank=True)
    smb_username = models.CharField(max_length=50, null=True, blank=True)
    smb_password = models.CharField(max_length=250, null=True, blank=True)
    smb_fstab_options = models.CharField(max_length=350, null=True, blank=True)

    def __str__(self):
        return self.name