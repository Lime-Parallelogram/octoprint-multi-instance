from volumes.models import Volume
from rest_framework import serializers

class VolumeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Volume
        fields = "__all__"