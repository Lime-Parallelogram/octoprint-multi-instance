from django.shortcuts import render
from rest_framework import viewsets

from volumes.models import Volume
from volumes.serializers import VolumeSerializer

# Create your views here.
class VolumeViewSet(viewsets.ModelViewSet):
    queryset = Volume.objects.all()
    serializer_class = VolumeSerializer