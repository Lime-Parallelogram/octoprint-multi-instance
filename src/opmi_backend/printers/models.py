from django.db import models

from volumes.models import Volume

# Create your models here.
class Printer(models.Model):
    # ID is generated automatically
    name = models.CharField(max_length=50)
    initial = models.CharField(max_length=2)
    colour = models.CharField(max_length=7) # Colour stored in hex format with preceding #
    
    octoprint_hostname = models.CharField(max_length=255)
    octoprint_port = models.IntegerField()

    serial_port_path = models.CharField(max_length=512, null=True, blank=True)
    serial_port_cgroup = models.IntegerField(null=True, blank=True)

    is_managed = models.BooleanField(default=False)
    automatic_theme_mode = models.IntegerField(default=0)

    volumes = models.ManyToManyField(Volume, blank=True)

    def __str__(self):
        return self.name