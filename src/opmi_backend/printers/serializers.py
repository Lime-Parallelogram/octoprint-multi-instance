from printers.models import Printer
from rest_framework import serializers
import json
from docker import errors as dockerErrors

from printers.container_management import OctoContainer

class PrinterSerializer(serializers.ModelSerializer):
    class Meta:
        model = Printer
        fields = "__all__"
    
    def is_managed_update_or_create(self):
        pass

    def create(self, validated_data):
        instance = super().create(validated_data)
        
        if validated_data.get("is_managed"):
            self.update_or_create(instance)
 
        return instance
    
    def update(self, instance, validated_data):
        old_instance = Printer.objects.get(pk=instance.id)
        old_volumes = list(old_instance.volumes.all()) # Need to save this list now or else it will be replaced with the updated one

        updated_instance = super().update(instance, validated_data)

        if validated_data.get("is_managed"):
            # Only recreate if container properties change
            recreate = ((old_instance.octoprint_port != updated_instance.octoprint_port) or
                        (old_instance.serial_port_path != updated_instance.serial_port_path) or
                        (old_volumes != list(updated_instance.volumes.all())) or
                        bool(self.context.get("request").query_params.get("force_recreate", False)))
            
            self.update_or_create(updated_instance, recreate)
            
        return updated_instance
    
    # delete behavior defined in views.py
    
    def update_or_create(self, instance, recreate=False):
        octo_container = OctoContainer(instance.id)

        try:
            if not octo_container.exists():
                octo_container.create_container(instance.octoprint_port, instance.serial_port_path, instance.serial_port_cgroup, instance.volumes)

            if recreate: # Recreate if volumes changed
                octo_container.delete_container()
                octo_container.create_container(instance.octoprint_port, instance.serial_port_path, instance.serial_port_cgroup, instance.volumes)
            
            elif not octo_container.running():
                octo_container.start()
            
            if instance.automatic_theme_mode > 0:
                octo_container.update_essentials()
                octo_container.update_serial(instance.serial_port_path)
                octo_container.write_config()

        except OctoContainer.ContainerNoConfigException:
            raise serializers.ValidationError({"field": "unknown", 
                                               "message": "Unable to update container config. The system may just be slow, refresh the page and attempt to recreate container if necessary"})
                                               
        except dockerErrors.APIError as e:
                if "port is already allocated" in str(e):
                    error = {"field": "octoprint_port", "message": "Invalid Port: Already in use"}
                else:
                    error = {"field": "unknown", "message": "Unknown Error"}

                print(e)
                
                raise serializers.ValidationError(error)