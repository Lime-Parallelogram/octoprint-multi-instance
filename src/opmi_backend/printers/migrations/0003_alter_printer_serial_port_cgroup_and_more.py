# Generated by Django 5.0 on 2024-01-01 16:49

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("printers", "0002_printer_automatic_theme_mode_printer_is_managed"),
    ]

    operations = [
        migrations.AlterField(
            model_name="printer",
            name="serial_port_cgroup",
            field=models.IntegerField(null=True),
        ),
        migrations.AlterField(
            model_name="printer",
            name="serial_port_path",
            field=models.CharField(max_length=512, null=True),
        ),
    ]
