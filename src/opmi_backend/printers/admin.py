from django.contrib import admin

from printers.models import Printer

# Register your models here.
admin.site.register(Printer)