from django.shortcuts import render
from rest_framework import viewsets

from printers.models import Printer
from printers.serializers import PrinterSerializer
from printers.container_management import OctoContainer

# Create your views here.
class PrinterViewSet(viewsets.ModelViewSet):
    queryset = Printer.objects.all()
    serializer_class = PrinterSerializer

    def destroy(self, request, *args, **kwargs):
        """Called when a printer is deleted"""
        printer_object = self.get_object()
        if printer_object.is_managed:
            octo_container = OctoContainer(printer_object.id)
            if octo_container.exists():
                octo_container.delete_container(volume_remove=True)
        
        return super().destroy(self, request, *args, **kwargs)