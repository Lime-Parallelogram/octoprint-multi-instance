import docker
import tarfile
import yaml
import copy
import time

from io import BytesIO

docker_client = docker.from_env()

class OctoContainer:
    class ContainerNotFoundException(docker.errors.APIError):
        """Error raised when an attempt is made to use methods that rely on container existing"""
        def __str__(self):
            return "The container does not exist. Create it first"
    
    class ContainerNoConfigException(docker.errors.APIError):
        """Error raised when an attempt is made to modify configuration before first startup"""
        def __str__(self):
            return "Could not load OctoPrint configuration files from container. Ensure the container has run at least once."

    def requires_container(func):
        def inner(self, *args, **kwargs):
            if not self.exists():
                raise self.ContainerNotFoundException("Container does not exist")
            
            return func(self, *args, **kwargs)
        return inner

    def requires_config(func):
        def inner(self, *args, **kwargs):
            if not self.has_config():
                raise self.ContainerNoConfigException("Container does not contain config files")
            
            return func(self, *args, **kwargs)
        return inner
    
    def __init__(self, printer_id, create=True):
        self.PRINTER_ID = printer_id
        self.CONTAINER = None
        
        for container in docker_client.containers.list(all=True):
            if container.labels.get("uk.limeparallelogram.opmi.printer_id") == str(self.PRINTER_ID):
                self.CONTAINER = container
                try:
                    self.current_configuration = self.read_config() # Stores existing printer configuration
                    self.original_config = copy.deepcopy(self.current_configuration)

                except docker.errors.APIError as e: 
                    if "Could not find the file /octoprint/octoprint/config.yaml" in str(e):
                        pass # Could occur if the container has been created but never started.
                             # In this case, we will treat the container as if it doesn't exist
                    else:
                        raise e
                break
        
    def exists(self):
        """Checks if there exists a container running on this system"""
        return self.CONTAINER != None
    
    def has_config(self):
        """Confirms if the initializer was able to load the config.yaml from container."""
        return hasattr(self, "original_config")
    
    def running(self):
        if not self.exists():
            return False
        return self.CONTAINER.status == "running"
    
    def create_container(self, port=None, serial_port_path=None, serial_port_cgroup=None, volumes=None):
        """Creates a new OctoPrint container with specified settings"""
        additional_volumes = {}
        for volume in volumes.all():
            additional_volumes[volume.host_path] = {"bind": volume.container_path}
        
        primary_volume_name = f"octoprint{self.PRINTER_ID}"
        try:
            current_volume = docker_client.volumes.get(primary_volume_name)
        except docker.errors.NotFound:
            docker_client.volumes.create(primary_volume_name)
        
        device_cgroup_rules = [f"c {serial_port_cgroup}:* rwm"] if serial_port_cgroup != 0 else []

        self.CONTAINER = docker_client.containers.run(
            detach=True,
            image="octoprint/octoprint:latest",
            name=f"octoprint{self.PRINTER_ID}",
            labels={"uk.limeparallelogram.opmi.printer_id":str(self.PRINTER_ID)},
            restart_policy={"Name": "always"},
            ports={"80":str(port)},
            device_cgroup_rules=device_cgroup_rules,
            volumes={"/dev": {"bind": "/dev"},
                     primary_volume_name: {"bind": "/octoprint"}} | additional_volumes
        )

        for i in range(10):  # Allow up to 10 attempts to read configuration. Container needs time to start
            try:
                self.current_configuration = self.read_config() # Stores existing printer configuration
                self.original_config = copy.deepcopy(self.current_configuration)
            except docker.errors.APIError:
                time.sleep(5)
            else:
                break
    
    @requires_container
    def start(self):
        """Start the current container"""
        self.CONTAINER.start()
    
    @requires_container
    def delete_container(self, volume_remove=False):
        """Removes the OctoPrint container"""
        if self.CONTAINER is not None:
            self.CONTAINER.stop()
            self.CONTAINER.remove()
            
            # Delete associated primary volume
            if volume_remove:
                try:
                    docker_client.volumes.get(f"octoprint{self.PRINTER_ID}").remove()
                except docker.errors.NotFound:
                    pass
    
    @requires_container
    def read_config(self):
        """Reads existing OctoPrint configuration from container"""
        # Extract .tar file from container that contains important config
        conf_bits, conf_stat = self.CONTAINER.get_archive("/octoprint/octoprint/config.yaml")
        file_obj = BytesIO()
        for i in conf_bits:
            file_obj.write(i)
        file_obj.seek(0)

        # Decode .tar into a text file reader
        tar = tarfile.open(mode='r', fileobj=file_obj)
        text = tar.extractfile("config.yaml")
        tar.close()

        # Decode yaml text into a python dictionary object
        return yaml.safe_load(text)
    
    @requires_config
    def write_config(self):
        """Writes updated configuration into container"""

        if self.current_configuration != self.original_config:
            file_data = yaml.safe_dump(self.current_configuration).encode("utf8")

            # Rebuild tar file
            new_tarstream = BytesIO()
            new_tar = tarfile.TarFile(fileobj=new_tarstream, mode="w")

            tarinfo = tarfile.TarInfo(name="config.yaml")
            tarinfo.size = len(file_data)
            tarinfo.mtime = time.time()
            
            new_tar.addfile(tarinfo, BytesIO(file_data))
            new_tar.close()
            new_tarstream.seek(0)

            # Add new file back to container and restart
            put_success = self.CONTAINER.put_archive("/octoprint/octoprint/", new_tarstream)
            self.CONTAINER.restart()

    @requires_config
    def update_essentials(self):
        """Updates basic settings in OctoPrint that must be used to support OctoPrint multi-instance"""

        # Enable iFrame support if not enabled 
        if not self.current_configuration["server"].get("allowFraming"):
            self.current_configuration["server"]["allowFraming"] = True
    
    @requires_config
    def update_serial(self, serial_port_path):
        """Updates OctoPrint's additional serial ports' to include specified serial port"""
        
        current_serial_settings = self.current_configuration.get("serial", {})
        if "/dev/serial/by-path" in serial_port_path: # Handle by-path mode
            current_serial_settings["additionalPorts"] = [serial_port_path]
            current_serial_settings["blacklistedPorts"] = ["/dev/tty*"]
        else: # Handle serial ports like /dev/ttyUSB0
            if (current_serial_settings.get("additionalPorts")):
                del current_serial_settings["additionalPorts"]
            current_serial_settings["blacklistedPorts"] = ["/dev/ttyS*"]
        
        self.current_configuration["serial"] = current_serial_settings