from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response

import os

class SerialPortList(APIView):
    def get(self, request):
        BY_PATH = True if request.query_params.get("by_path", "false") == "true" else False # by_path mode indicates that devices should be searched for in /dev/serial/by-path directory
        BASE_PATH = "/dev/serial/by-path/" if BY_PATH else "/dev/"

        relevant_devices = []
        try:
            for device in os.listdir(BASE_PATH):
                # All devices in by_path mode are valid. Only ttyACM and ttyUSB devices are valid from the /dev directory
                if BY_PATH or ("ttyACM0" in device or "ttyUSB0" in device):
                    device_path = BASE_PATH+device
                    cgroup = os.major(os.stat(device_path).st_rdev)

                    relevant_devices.append({
                        "full_path": device_path,
                        "cgroup": cgroup
                    })
        
        except FileNotFoundError: # If no devices are connected, /dev/serial does not exist
            pass

        return Response(relevant_devices)