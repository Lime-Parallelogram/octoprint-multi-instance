import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { EventEmitter, Injectable, Output } from '@angular/core';
import { throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

export interface Volume {
  id: number,
  name: string,
  container_path: string,
  host_path: string,

  smb: boolean,
  smb_path?: string | undefined,
  smb_username?: string | undefined,
  smb_password?:  string | undefined,
  smb_fstab_options?: string | undefined
};

@Injectable({
  providedIn: 'root'
})
export class VolumesService {
  server_volumes: Array<Volume> = [];

  @Output() volumes_changed = new EventEmitter<Array<Volume>>() // Creates event when printer list is updated

  constructor(private client: HttpClient) {
    this.refresh_volumes() // Request list of printers from server on page load
  }

  //^ ================= Printer List ================= ^//
  // Update local printer list to match that which is stored on the server
  refresh_volumes () {
    this.client.get<Array<Volume>>("/api/volumes").subscribe((data) => {
      this.server_volumes = data;
      this.volumes_changed.emit(this.server_volumes)
    });
  }

  // Delete specified printer from server
  delete(volume_id: number) {
    return this.client.delete("/api/volumes/"+volume_id, {observe: "response"}).pipe(
      tap(() => {
        this.refresh_volumes()
      })
    )
  }

  // Update stored configuration on server
  update(volume: Volume) {
    return this.client.put("/api/volumes/"+volume.id+"/", volume, {observe: "response"})
    .pipe(
      catchError((error:HttpErrorResponse) => {
        if (error.status == 404) {
          return this.client.post("/api/volumes/", volume, {observe: "response"})
        } 

        return throwError(() => error);
      }),
      tap(() => {
        this.refresh_volumes()
      })
    )
  }
}
