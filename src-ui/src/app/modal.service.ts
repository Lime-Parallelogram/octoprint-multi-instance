import { Injectable } from '@angular/core';
import { Printer, PrintersService, SerialPort } from './printers.service';
import { Observable, Subject, catchError, finalize, tap } from 'rxjs';
import { Volume, VolumesService } from './volumes.service';
import { NgxSpinnerService } from "ngx-spinner";
import { HttpErrorResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ModalService {

  constructor(private printer_service: PrintersService, private volume_service: VolumesService, private spinner: NgxSpinnerService) { }

  printer_edit_open = false;
  printer_edit_editing: Printer | undefined;

  editPrinter(printer: Printer) {
    this.printer_edit_editing = printer;
    this.printer_edit_open = true;
  }

  editPrinterClose() {
    this.printer_edit_open = false;
  }

  editPrinterSave(printer: Printer) {
    this.spinner.show()
    return this.printer_service.update_printer(printer).pipe(
      finalize(() => {
        this.spinner.hide()
      })
    )
  }

  // Volume edit modal
  volume_edit_open = false;
  volume_edit_editing: Volume | undefined;

  editVolume(volume: Volume) {
    this.volume_edit_editing = volume;
    this.volume_edit_open = true;
  }

  editVolumeClose() {
    this.volume_edit_open = false;
  }

  editVolumeSave(volume: Volume) {
    this.volume_service.update(volume).subscribe()
    this.volume_edit_open = false;
    
  }

  // Serial Port Selector
  serial_picker_open = false;
  private serialPickerSource = new Subject<SerialPort>();
  
  selectSerialPort(): Observable<SerialPort> {
    this.serial_picker_open = true;
    return this.serialPickerSource
  }

  closeSerialPicker(serial_port: SerialPort | undefined) {
    if (serial_port) { // Undefined indicates close clicked
      this.serialPickerSource.next(serial_port)
    }
    this.serial_picker_open = false;
  }
}
