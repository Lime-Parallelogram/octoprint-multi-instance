import { Injectable, Output, EventEmitter } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http'
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { catchError, tap, throwError } from 'rxjs';

export interface SerialPort {
  full_path: string,
  cgroup: number
}

export interface Printer {
  id: number,
  name: string,
  initial: string,
  octoprint_hostname?: string | undefined,
  octoprint_port?: string | undefined
  serial_port_path?: string | undefined,
  serial_port_cgroup?: number | undefined,
  colour: string,
  default?: boolean | undefined,
  is_managed: boolean,
  automatic_theme_mode: number,
  volumes: Array<number>
}

export interface ContainerUpdateResponse {
  config_changes?: boolean,
  error?: string
}

export interface PrinterErrorResponse {
  field: string,
  message: string
}

@Injectable({
  providedIn: 'root'
})
export class PrintersService {
  active_octoprint_url: SafeResourceUrl|undefined;
  server_printers: Array<Printer> = []; // Stores list of printers that are saved on server. Should match at all times.

  @Output() printers_changed = new EventEmitter<Array<Printer>>() // Creates event when printer list is updated

  constructor(private dom_sanitizer: DomSanitizer, private router: Router, private client: HttpClient) {
    this.refresh_printers() // Request list of printers from server on page load
  }

  display_octoprint (octo_url: string) {
    this.active_octoprint_url = this.dom_sanitizer.bypassSecurityTrustResourceUrl(octo_url);

    this.router.navigate([""]);
  }

  //^ ================= Printer List ================= ^//
  // Update local printer list to match that which is stored on the server
  refresh_printers () {
    this.client.get<Array<Printer>>("/api/printers").subscribe((data) => {
      this.server_printers = data;
      this.printers_changed.emit(this.server_printers)
    });
  }

  // Delete specified printer from server
  delete_printer(printer_id: number) {
    return this.client.delete("/api/printers/"+printer_id, {observe: "response"}).pipe(
      tap(() => {
        this.refresh_printers()
      })
    )
  }

  // Update stored configuration on server
  update_printer(printer: Printer, force_recreate=false) {
    return this.client.put("/api/printers/"+printer.id+"/", printer, {observe: "response", params: {force_recreate: force_recreate}})
    .pipe(
      catchError((error:HttpErrorResponse) => {
        if (error.status == 404) {
          return this.client.post("/api/printers/", printer, {observe: "response"})
        } 

        return throwError(() => error);
      }),
      tap(() => {
        this.refresh_printers()
      })
    )
  }

  // Find printer with specified id in printer_list
  search_printers (id: number) : Printer|false {
    for (let i=0; i<this.server_printers.length; i++) {
      if (id == this.server_printers[i].id) {
        return this.server_printers[i]
      }
    }
    return false
  }

  //^ ================== Serial Ports ================== ^//
  // Request list of available server ports from server
  get_serial_ports (by_path_mode: boolean) {
    let params = {
      by_path: by_path_mode
    }
    return this.client.get<Array<SerialPort>>("/api/serial/get_serial", {params: params});
  }

  //^ ============== Container Management ============== ^//
  // Ask server to perform necessary updates on container
  update_container(printer:Printer) {
    return this.client.post<ContainerUpdateResponse>("/api/printers/update_container", {"printer": printer}, {params: {id: printer.id}, observe: "response"})
  }
}
