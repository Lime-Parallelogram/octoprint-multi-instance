import { Component, EventEmitter, Output } from '@angular/core';
import { faRefresh, faSquareXmark } from '@fortawesome/free-solid-svg-icons';
import { ModalService } from 'src/app/modal.service';
import { PrintersService, SerialPort } from 'src/app/printers.service';

@Component({
  selector: 'app-serial-modal',
  templateUrl: './serial-modal.component.html',
  styleUrls: ['./serial-modal.component.scss', '../modal_styles.scss','../../widgets/lists/list_styles.scss']
})
export class SerialModalComponent {
  faClose = faSquareXmark;
  faRefresh = faRefresh;

  available_serial_ports: Array<SerialPort> = [{full_path: "/dev/null", cgroup: 188}]
  new_serial_ports: Array<SerialPort> = []
  by_path_mode: boolean = false;
  selected_serial_port: SerialPort|undefined;

  @Output() close = new EventEmitter();

  constructor (private printer_service: PrintersService, public modal_service: ModalService) {}

  ngOnInit() {
    this.refreshSerialPorts()
  }

  refreshSerialPorts(show_new = false) {
    let old_serial_ports = this.available_serial_ports
    this.printer_service.get_serial_ports(this.by_path_mode).subscribe((data) => {
      this.available_serial_ports = data;
      if (show_new) {
        this.new_serial_ports = this.available_serial_ports.filter(item => {
          for (let old_serial_port of old_serial_ports) { // Need to test based on full path because no two objects are equal
            if (old_serial_port.full_path == item.full_path) { return false }
          }
        return true;
      })
      }
    })
  }

}
