import { Component } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ModalService } from 'src/app/modal.service';

@Component({
  selector: 'app-settings-volume-edit-modal',
  templateUrl: './settings-volume-edit-modal.component.html',
  styleUrls: ['./settings-volume-edit-modal.component.scss', '../modal_styles.scss', '../../settings/settings_global.scss']
})
export class SettingsVolumeEditModalComponent {
  volumeReactiveForm: FormGroup = new FormGroup({});

  constructor (public modal_service: ModalService, private fb: FormBuilder) {}

  ngOnInit() {
    if (this.modal_service.volume_edit_editing) {
      this.volumeReactiveForm = this.fb.group(this.modal_service.volume_edit_editing)
    }
  }

  saveClick() {
    this.modal_service.editVolumeSave(this.volumeReactiveForm.value)
  }
}
