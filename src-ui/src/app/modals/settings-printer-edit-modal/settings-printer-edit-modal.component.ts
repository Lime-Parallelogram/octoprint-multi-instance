import { HttpErrorResponse } from '@angular/common/http';
import { Component, EventEmitter, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { catchError, of, throwError } from 'rxjs';
import { ModalService } from 'src/app/modal.service';
import { PrinterErrorResponse } from 'src/app/printers.service';
import { VolumesService } from 'src/app/volumes.service';

@Component({
  selector: 'app-settings-printer-edit-modal',
  templateUrl: './settings-printer-edit-modal.component.html',
  styleUrls: ['./settings-printer-edit-modal.component.scss', '../modal_styles.scss', '../../settings/settings_global.scss']
})
export class SettingsPrinterEditModalComponent {

  printerReactiveForm: FormGroup = new FormGroup({});
  errorResponseText: string = "";

  constructor (public modal_service: ModalService, public volume_service: VolumesService, private fb: FormBuilder) {}

  @Output() close = new EventEmitter()
  @Output() save = new EventEmitter()

  ngOnInit() {
    if (this.modal_service.printer_edit_editing) {
      this.printerReactiveForm = this.fb.group({
        id: this.modal_service.printer_edit_editing.id,
        name: [this.modal_service.printer_edit_editing.name, Validators.required],
        initial: [this.modal_service.printer_edit_editing.initial, Validators.required],
        octoprint_hostname: [this.modal_service.printer_edit_editing.octoprint_hostname, Validators.required],
        octoprint_port: [this.modal_service.printer_edit_editing.octoprint_port, Validators.required],
        serial_port_path: this.modal_service.printer_edit_editing.serial_port_path,
        serial_port_cgroup: this.modal_service.printer_edit_editing.serial_port_cgroup,
        colour: this.modal_service.printer_edit_editing.colour,
        is_managed: this.modal_service.printer_edit_editing.is_managed,
        automatic_theme_mode: this.modal_service.printer_edit_editing.automatic_theme_mode,
        volumes: [this.modal_service.printer_edit_editing.volumes]
      })
    }
  }

  serialOpen() {
    // Update serial port value when modal closed
    this.modal_service.selectSerialPort().subscribe((sp) => {
      this.printerReactiveForm.patchValue({
        serial_port_path: sp.full_path,
        serial_port_cgroup: sp.cgroup
      })
    })
  }

  saveClick() {
    this.errorResponseText = "";
    
    if (this.printerReactiveForm.valid) {
      this.modal_service.editPrinterSave(this.printerReactiveForm.value).pipe(
        catchError((error:HttpErrorResponse) => {
          if (error.status == 400) {
            this.errorResponseText = (<PrinterErrorResponse>error.error).message;
            if ((<PrinterErrorResponse>error.error).field != "unknown") {
              this.printerReactiveForm.controls[(<PrinterErrorResponse>error.error).field].setErrors({"problems": true})
            } else {
              this.printerReactiveForm.setErrors({"problems": true})
            }  
          }

          return throwError(() => error);
          
        }),
      ).subscribe(() => { this.modal_service.editPrinterClose(); });
    }
    
  }
  
}
