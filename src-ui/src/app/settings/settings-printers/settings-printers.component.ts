import { Component } from '@angular/core';
import { PrinterErrorResponse, SerialPort } from 'src/app/printers.service';
import { Printer, PrintersService } from 'src/app/printers.service';
import { Clipboard } from '@angular/cdk/clipboard';
import { SettingServiceService, VolumeDefinition } from 'src/app/setting-service.service';
import * as yaml from 'yaml';
import { faExclamationCircle, faFileExport, faFloppyDisk, faTrashCan, faTrash, faBars, faPencil, faRotateLeft, faCube } from '@fortawesome/free-solid-svg-icons';
import { AbstractControl, FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ModalService } from 'src/app/modal.service';
import { catchError, finalize, throwError } from 'rxjs';
import { NgxSpinnerService } from "ngx-spinner";
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-settings-printers',
  templateUrl: './settings-printers.component.html',
  styleUrls: ['./settings-printers.component.scss', '../../widgets/lists/list_styles.scss']
})
export class SettingsPrintersComponent {

  constructor (public printer_service: PrintersService, public modal_service: ModalService, private setting_service: SettingServiceService, private clipboard: Clipboard, private fb: FormBuilder, private spinner: NgxSpinnerService) {}

  printers: Array<Printer> = [];

  // FontAwesome icons used in template
  faFloppyDisk = faFloppyDisk;
  faRequiredIcon = faExclamationCircle;
  faUpdateContainer = faFileExport;
  faDeleteIcon = faTrashCan;
  faHandle = faBars;
  faEdit = faPencil;
  faReset = faRotateLeft;
  faError = faExclamationCircle;
  faManaged = faCube;

  // Create form using Reactive form Syntax
  printerReactiveForm: FormGroup = new FormGroup({
    printer_form_list: new FormArray([
      
    ])
  })

  // For quick access to printer_form_list as correct type
  get printer_form_list() {
    return (this.printerReactiveForm.get("printer_form_list") as FormArray)
  }

  // Create a new FormGroup object from provided printer
  createPrinterAccordionEntry(printer: Printer) {
    return this.fb.group({
      id: printer.id,
      name: printer.name,
      initial: printer.initial,
      colour: printer.colour,
      octoprint_hostname: [printer.octoprint_hostname, Validators.required],
      octoprint_port: [printer.octoprint_port, Validators.required],
      serial_port_path: [printer.serial_port_path, Validators.required],
      serial_port_cgroup: [printer.serial_port_cgroup, Validators.required]
    })
  }

  // Create form from standard Printer array
  buildForm(printers: Array<Printer>) {
    printers.forEach(printer => {
        this.printer_form_list.push(this.createPrinterAccordionEntry(printer));
      })
  }
  
  //^ Component Methods ^//
  ngOnInit() {
    this.printers = this.printer_service.server_printers;
    this.printer_service.printers_changed.subscribe(printers => {
      this.buildForm(printers)
    })
  }

  // Runs on add button click
  addPrinter() {
    // Find new id
    let largest_id = 0;
    this.printer_service.server_printers.forEach((printer) => {
      if (printer.id > largest_id) { largest_id = printer.id }
    })

    let new_id = largest_id + 1;
    let new_port = 5000 + new_id;

    let new_printer: Printer = {
      id: new_id,
      name: "New Printer",
      octoprint_hostname: "localhost",
      octoprint_port: new_port.toString(),
      initial: "N",
      colour: "default",
      serial_port_path: "",
      serial_port_cgroup: 0,
      is_managed: false,
      automatic_theme_mode: 0,
      volumes: [],

    }
    
    this.modal_service.editPrinter(new_printer)
  }

  // Remove printer from form list
  deletePrinter(printer: Printer) {
    if (confirm("You are about to remove this printer. \nIf this is a managed instance, the associated OctoPrint container will be deleted.")) {
      this.spinner.show()
      this.printer_service.delete_printer(printer.id).pipe(
        finalize(() => {
          this.spinner.hide()
        })
      ).subscribe()
    }
  }

  // Recreate docker container if managed
  recreatePrinter(printer: Printer) {
    if (confirm("This will re-create and restart the OctoPrint instance. Are you sure?")) {
      this.spinner.show()
      this.printer_service.update_printer(printer, true).pipe(
        finalize(() => {
          this.spinner.hide()
        }),
        catchError((error:HttpErrorResponse) => {
          if (error.status == 400) {
            alert("Error recreating container!\n" + (<PrinterErrorResponse>error.error).message);
          }

          throw throwError(() => error)
        })
      ).subscribe();
    }
  }
    
}
