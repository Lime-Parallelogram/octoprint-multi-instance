import { Component, Input, Output } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SettingsComponent } from '../settings/settings.component';

export type tab = {
  id: string,
  name: string,
  router_link: string
}
@Component({
  selector: 'app-settings-tabs',
  templateUrl: './settings-tabs.component.html',
  styleUrls: ['./settings-tabs.component.scss']
})
export class SettingsTabsComponent {
  @Input() available_tabs: Array<tab> = [];
  @Input() selected_tab: string = "";

  constructor (private router: Router, private activated_route: ActivatedRoute) {}
  
  ngDoCheck() {
    if (this.router.url == "/settings") { // Only redirect when actually at /settings so as to avoid infinite redirect loop
      this.router.navigate( [this.selected_tab], {relativeTo: this.activated_route} )
    }
    
  }
}
