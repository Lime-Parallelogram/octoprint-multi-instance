import { Component } from '@angular/core';
import { AbstractControl, FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { faExclamationCircle, faTrashCan, faPencil, faBars } from '@fortawesome/free-solid-svg-icons';
import { ModalService } from 'src/app/modal.service';
import { VolumeDefinition, DockerSettings, SettingServiceService } from 'src/app/setting-service.service';
import { Volume, VolumesService } from 'src/app/volumes.service';
import * as yaml from 'yaml';

@Component({
  selector: 'app-settings-docker',
  templateUrl: './settings-docker.component.html',
  styleUrls: ['./settings-docker.component.scss', '../../widgets/lists/list_styles.scss']
})
export class SettingsDockerComponent {
  // FontAwesome Icons
  faDeleteIcon = faTrashCan;
  faEditIcon = faPencil;
  faHandle = faBars;
  faRequiredIcon = faExclamationCircle;

  constructor (public volume_service: VolumesService, public modal_service: ModalService, private fb: FormBuilder) {}

  ngOnInit() {
    
  }

  deleteVolume(volume: Volume) {
    this.volume_service.delete(volume.id).subscribe()
  }

  addVolume() {
    // Find new id
    let largest_id = 0;
    this.volume_service.server_volumes.forEach((volume) => {
      if (volume.id > largest_id) { largest_id = volume.id }
    })

    let new_id = largest_id + 1;

    let new_volume: Volume = {
      id: new_id,
      name: "New Volume",
      host_path: "",
      container_path: "/octoprint/octoprint/uploads",
      smb: false

    }
    
    this.modal_service.editVolume(new_volume)
  }

}
