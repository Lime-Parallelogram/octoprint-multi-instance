import { Component, Input } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { PrintersService } from 'src/app/printers.service';

@Component({
  selector: 'app-octoprint-view',
  templateUrl: './octoprint-view.component.html',
  styleUrls: ['./octoprint-view.component.scss']
})
export class OctoprintViewComponent {

  constructor (public printer_service: PrintersService) {}
}
