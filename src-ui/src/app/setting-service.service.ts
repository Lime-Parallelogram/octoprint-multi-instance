import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

export type VolumeDefinition = {
  name: string,
  container_path: string,
  additional_config: string
}

export interface DockerSettings {
  additional_volumes: Array<VolumeDefinition>
}

@Injectable({
  providedIn: 'root'
})
export class SettingServiceService {

  constructor(private client: HttpClient) { }

  get_docker_settings() {
    return this.client.get<DockerSettings>("/api/docker/settings")
  }

  save_docker_settings(new_settings: DockerSettings) {
    return this.client.post("/api/docker/settings", new_settings, {observe: "response"})
  }
}
