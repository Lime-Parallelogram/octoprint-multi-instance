import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OctoprintViewComponent } from './main/octoprint-view/octoprint-view.component';
import { SettingsComponent } from './settings/settings/settings.component';
import { SettingsPrintersComponent } from './settings/settings-printers/settings-printers.component';
import { SettingsDockerComponent } from './settings/settings-docker/settings-docker.component';

const routes: Routes = [
  {path: "", component: OctoprintViewComponent},
  {path: "settings", component: SettingsComponent, children: [
    {
      path: "printers",
      component: SettingsPrintersComponent
    },
    {
      path: "docker_settings",
      component: SettingsDockerComponent
    }
  ]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
