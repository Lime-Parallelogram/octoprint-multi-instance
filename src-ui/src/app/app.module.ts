import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SidebarComponent } from './sidebar/sidebar/sidebar.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { PrinterChipComponent } from './sidebar/printer-chip/printer-chip.component';
import { OctoprintViewComponent } from './main/octoprint-view/octoprint-view.component';
import { HttpClientModule, HttpClientXsrfModule } from '@angular/common/http';
import { SettingsComponent } from './settings/settings/settings.component';
import { SettingsTabsComponent } from './settings/settings-tabs/settings-tabs.component';
import { SettingsPrintersComponent } from './settings/settings-printers/settings-printers.component';
import { SerialModalComponent } from './modals/serial-modal/serial-modal.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SettingsDockerComponent } from './settings/settings-docker/settings-docker.component';
import { SettingsPrinterEditModalComponent } from './modals/settings-printer-edit-modal/settings-printer-edit-modal.component';
import { SettingsVolumeEditModalComponent } from './modals/settings-volume-edit-modal/settings-volume-edit-modal.component';
import { NgxSpinnerModule } from "ngx-spinner";

@NgModule({
  declarations: [
    AppComponent,
    SidebarComponent,
    PrinterChipComponent,
    OctoprintViewComponent,
    SettingsComponent,
    SettingsTabsComponent,
    SettingsPrintersComponent,
    SerialModalComponent,
    SettingsDockerComponent,
    SettingsPrinterEditModalComponent,
    SettingsVolumeEditModalComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    NgxSpinnerModule,
    HttpClientModule,
    HttpClientXsrfModule.withOptions({
      cookieName: 'csrftoken',
      headerName: 'X-CSRFTOKEN',
    }),
    AppRoutingModule,
    FontAwesomeModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
