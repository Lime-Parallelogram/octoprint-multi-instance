import { Component, Input } from '@angular/core';
import { PrintersService } from 'src/app/printers.service';

@Component({
  selector: 'app-printer-chip',
  templateUrl: './printer-chip.component.html',
  styleUrls: ['./printer-chip.component.scss']
})
export class PrinterChipComponent {
  @Input() colour = "#000000";
  @Input() initial = "0"
  @Input() octo_url = ""

  constructor (private printer_service: PrintersService) {}

  on_click() {
    this.printer_service.display_octoprint(this.octo_url)
  }

}
