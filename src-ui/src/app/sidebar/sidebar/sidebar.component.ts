import { Component, OnInit } from '@angular/core';
import { faGear } from '@fortawesome/free-solid-svg-icons';
import { PrintersService } from 'src/app/printers.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  faGear = faGear;
  
  constructor(public printer_service: PrintersService) { }

  ngOnInit(): void {
    
  }

}
