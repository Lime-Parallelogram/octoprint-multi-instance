# OctoPrint Multi-Instance Aggregator
OctoPrint is an excellent and extremely competent 3D printer control system that I have been using at home for a number of years. When my school technology department got themselves a number of new printers, I was more than happy to recommend OctoPrint as a control solution and have had an active part in setting up their system. Given that I am now only months away from leaving the school entirely, I want to ensure that my solution is as maintainable and extensible as possible - hence the following project.

## Current implementation
The current implementation involves a number of python virtual environments (venvs), into each of which OctoPrint is installed. All of this is installed onto a PC with a bare-bones installation of debian and a browser in kiosk mode. I created a simple HTML page that would display the different instances of OctoPrint in an iframe.

The key shortcoming of the current system is its lack of easy extensibility. It is necessary to manually create new virtual environments for each new printer you wish to add - a process that involves a number of terminal commands, file modification and a reasonable knowledge of the area. I also believe the configuration of having multiple python venvs under the same system is quite unusual - thus making more unlikely that OctoPrint community members could understand and assist with the set-up.

## Stakeholders
As previously mentioned, this is being developed primarily for my school tech department. They are confident with computers and will be able to use even very comprehensive user interfaces however they would feel less comfortable having to descend into a CLI environment. Both staff and students will be using this software for their important coursework so it is imperative that the system is both robust and reliable.

## Essential Functionality
1. **OctoPrint interface access**  
This requirement is for the user to have access to all functionality of each and every OctoPrint instance that is running
2. **Overview of current printer activity**  
Since you will only be looking at a single OctoPrint page, you cannot see at a glace the stage that all of the printers are at. There should be an area that will give a summary of all currently active printers.
3. **Independent Restart**  
If ever an OctoPrint installation goes wrong or crashes, it may be necessary to restart it. It should be possible to do this easily and without interrupting any ongoing jobs.
4. **Printer Serial Ports**  
It must be possible to identify connected printers via the serial port they are connected to. In the system such as that in my school, the only way to differentiate the printers from one another is by the physical USB they are plugged into. It should be possible to automatically find the full path of new printers when they are plugged in
5. **Adding printers**  
It is very likely that users of the system will want to add more printers to their setup in the future. Therefore, it is important that they can easily scale up the number of printers that are connected. This should include the automatic configuration of parameters like name and colour within OctoPrint.
6. **Removing printers**  
Since there is functionality to add new printers to the software, it follows logically that there should be functionality to remove them again
7. **Easy updates**  
Since the aim of the system is for support to continue after I have left the school, it must be easy for the users (teacher or IT admin) to update to newer releases of the system. This could be in response to bug reports or to changing technologies.
8. **Network printing**  
In many businesses / educational establishments, all files are stored centrally on a network. We should be able to connect to a network drive and make those files available to individual OctoPrint instances.
9. **Printer State Independence**  
This is an important feature to ensure user-friendliness and limit uncertainty in behavior. It must be possible for the system to start and function without requiring some/all printers to be in a specific state. This includes the ability to operate with printers off, on or unplugged

## Limitations
1. The system will not re-implement all functionality from the OctoPrint web interface but instead show the standard OctoPrint interface inside an iframe.
2. Windows and MacOS operating systems will not be officially supported. This is because OctoPrint itself does not natively support these systems.
3. Initial setup will still require manual work - likely involving the installation of docker and the building of a docker-compose file.

## Success Criteria
1. Interface should provide an overview of what all printers are currently doing. This should be visible at all times and should be visible without having to click onto any specific page.
2. An almost indefinite number instances of OctoPrint should be supported without interfering with one another
3. Teachers should be able to add and remove a new printer from scratch unaided (with only help from the documentation)
4. Device should be able to run reliably almost indefinitely without needing a machine restart or intervention with external applications (test duration will be 4 weeks)
5. The status of each OctoPrint instance should be clear to the user
6. A user should be able to start and restart individual containers in case they notice that they are experiencing errors
7. A student should be able to save a file that they wish to print on the shared folder, move to the printer controller and print it on any of the connected printers.

## Roadmap / Prototype plan
### -- Version 1
The first version of the application will aim to provide a functional replacement for the school's current implementation (as described above). In the repository, I will aim to provide detailed instructions on how to set up the operating system environment and install the necessary containerization tools. The v1 application will make use of pre-existing management solutions and components that may compromise on user experience and ease of use to ensure a full feature-set and reliable operation.

Version 1 will be comprised of:
- Setup instructions for a locked-down kiosk-mode operating system
- Setup instructions for a container environment compatible with OctoPrint
- Overview page with tabs to switch between all currently installed OctoPrint instances
- Administration page provided by third-party container-management solution that will allow the user to add, remove and restart OctoPrint containers
- Serial port viewer that will allow users to find the correct serial port for their printer. This will be needed in order to setup containers in the third-party container-management solution
- Manual setup of new OctoPrint installations within the freshly created containers
- A system that allows for easy update of the aggregator without having to manually re-install it
- Configuration page for the aggregator system. This page will need to have functionality for the following things
    - Manually adding an instance of OctoPrint by typing in its URL
    - Removing instances of OctoPrint
    - Setting the colour for an instance

### -- Version 2
Once version 1 has been reliably implemented and tested, I will begin to focus more on quality-of-life features for the system. This will include making the aggregator program aware of the containers that OctoPrint is running in (i.e. relying less on the 3rd-party container management solution to provide this information). Such an integration with the containers will reduce the need for the user to access the 3rd-party management page but not yet negate it entirely. The aggregator will also begin to make API connections to OctoPrint itself. This will be used to provide live information from each instances - such the file name or remaining time for a currently active job.

The main changes that will be made in version 2 are as follows:
- The aggregator system will now interface directly with the OctoPrint API. This will provide the following additional functionality
    - Automatic retrieval of the instance's colour
    - Display the name of the currently printing file to the user
    - Display the completion percentage of active jobs to the user  
- Implementation of the API connection will require an additional step when a new instance is added to the aggregator. The user will need to grant the aggregator API access. This can be done through an additional iframe that is displayed when the user first enters their OctoPrint URL.
- Container status information provided within the aggregator interface.
- Basic container controls available within the aggregator interface. This will include the ability to restart a running container or start a stopped one

### -- Version 3
Going forward, the third party container-management solution will be removed entirely. All necessary functionality will be integrated natively into the aggregator interface. This functionality must include
- Adding new instances of octoprint for new printers
- Deleting any instances of octoprint related to printers
- Re-Mapping the serial ports of a printer
- Updating the OctoPrint instances when a new version is released
- Managing connection to each of the printers
- Management of containers and printers can be locked behind password authentication
- Updating the master container if a new version is released  

Other features that may appear in going forward could be an increasing amount of control via the API - such as the ability to start and stop prints from the sidebar without needing to navigate into their interfaces.