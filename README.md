# OctoPrint Multi-Instance

![Project Status Badge showing beta](https://badgen.net/static/Project%20Status:/Beta%20Release/orange)

> A front-end aggregator system for multiple instances of OctoPrint. This project is developed for use in any situation in which you have single display for controlling multiple instances of OctoPrint (either each running on their own machine or with multiple instances running on the same local PC).

This project is being developed in coordination with my school who currently have 7 3D Printers, all of which are connected to a single PC running multiple instances of OctoPrint. Read about the full project overview and goals in [specification.md](specification.md).

---

### Screenshots:
The main view from which you can access OctoPrint:
![A screenshot showing the main OctoPrint interface](docs/img/screenshot_octoprint.png)

The settings page allows you to configure known printers:
![A screenshot showing the settings page for printer configuration](docs/img/screenshot_printer_settings.png)

### About:
OctoPrint Multi-Instance is being developed with the goal of providing a simple and coherent way to host multiple instances of OctoPrint from a single host machine; it is not intended to replace OctoPrint in any situation. The interface is designed to support being displayed in a kiosk mode browser so, once setup, all common tasks can be conducted from within the interface.

The program uses Docker to support running multiple OctoPrint instances on the same machine. OctoPrint Multi-Instance makes use of the Docker socket (`/var/run/docker.sock`) and therefore **does not** support installation on Microsoft Windows.

### Features:
- List all your 3D Printers on the sidebar and click between them to access associated OctoPrint dashboards
    - The colour and identifier initial are fully customizable
- Each printer has an associated serial port
    - Serial port picker supports 'by-path mode', allowing printers to be identified by which physical USB port they are connected to
    - Refresh indicator shows newly connected serial ports, making it easy to determine which serial port is associated with which printer
    - Containers restricted to access a single serial port. OctoPrint 'Additional Serial Ports' configuration is automatically updated as necessary
- Support for defining additional volumes for OctoPrint containers
    - Connect a volume to the OctoPrint uploads directory to share files between all containers
    - Point the host path to a mounted SMB drive to use network storage
- OctoPrint container management now integrated into add/remove printer UI

### Installation & System Setup:
If you already have a system setup onto which you would like to install OctoPrint Multi-Instance onto and existing system, make sure you have docker installed and check out [docs/install.md](docs/install.md).

If you would like to setup OctoPrint Multi-Instance and use it as an always-available kiosk, check out [docs/suggested_system.md](docs/suggested_system.md).

### Issues:
If you do encounter any issues while using this software, please see the [issues](https://gitlab.com/Lime-Parallelogram/octoprint-multi-instance/-/issues) page of the GitLab repository.

### Roadmap:
This section will discuss possible upcoming features for the project
- **Widescreen View**
In many ways, this is the flagship view. It contains the actual OctoPrint interface on the right with an expanded sidebar on the left. The expanded sidebar will allow for extra information to be displayed about the printer. This could be the printer address as shown, the status of the printer or its ETA. Below is a mockup of  how this could look:

![OctoPrint Widescreen - Multiple printers](design/widescreen-multiple-printing.png)

- **API Connection / Progress**
The OctoPrint icons are contained within a shape that would perfectly support a funky progress bar. An idea of how this could look is displayed below. This would of course require the coordinator container to have access to the API for OctoPrint so this functionality would also need to be implemented

![Octopint 4:3 Screen Design - Multiple printers](design/43screen-multiple-printing.png)

- **Mobile View:**
At a later point, a dedicated mobile interface can be prepared to support accessing the printers from a mobile device